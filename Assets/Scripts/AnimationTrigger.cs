using Unity.VisualScripting;
using UnityEngine;

public class AnimationTrigger : MonoBehaviour
{
	public GameObject currentLevelPrefab;
	public GameObject nextLevelPrefab;
	private Animator animator;
	private GameObject player;
	private PlayerMovement playerMovement;
	private SpriteRenderer playerSprite;

	void Start()
	{
		animator = GetComponent<Animator>();
		animator.enabled = false;
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Player"))
		{
			player = other.GameObject();
			playerMovement = player.GetComponent<PlayerMovement>();
			playerSprite = player.GetComponent<SpriteRenderer>();

			playerMovement.canMove = false;
			
			animator.enabled = true;
			animator.Play(name);
		}
	}

	private void Update() {
		var state = animator.GetCurrentAnimatorStateInfo(0);
		if (state.IsName(name) && state.normalizedTime >= 1f)
		{
			if (name.ToLower().Contains("door"))
			{
				player.transform.position = new Vector3(-3.7f, 0, 0);
				playerMovement.canMove = true;

				Instantiate(nextLevelPrefab, currentLevelPrefab.transform.position, Quaternion.identity);
				Destroy(currentLevelPrefab);
			}
			else if (name.ToLower().Contains("cardboard"))
			{
				Destroy(this);
			}
		}
	}
}
