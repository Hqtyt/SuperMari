using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class BGFish : MonoBehaviour
{
	public float swimSpeed = 0.3f;
	public GameObject coralHomeObject;
	public float tetherDistance = 0.5f;
	public GameObject groundTilemapObject;
	public int assetsPixelsPerUnit = 32;
	public Vector2 referenceResolution = new(288, 160);

	private Vector2 positionBounds;
	private Vector2 randomDirection = Vector2.zero;
	private float timer = 0f;
	private Tilemap groundTilemap;
	private SpriteRenderer sprite;
	private readonly List<string> sortingLayers = new() {"BackgroundFish", "MiddlegroundFish", "ForegroundFish"};

	void Start()
	{
		positionBounds = (referenceResolution / assetsPixelsPerUnit) / 2f;
		positionBounds.y = 2.25f;
		sprite = GetComponent<SpriteRenderer>();
		groundTilemap = groundTilemapObject.GetComponent<Tilemap>();

		transform.position = coralHomeObject.transform.position;
	}

	void Update()
	{
		var targetPosition = transform.position + (Vector3)randomDirection * swimSpeed * Time.deltaTime;
		var tileExists = groundTilemap.HasTile(groundTilemap.WorldToCell(targetPosition));
		var tooFar = Vector3.Distance(coralHomeObject.transform.position, targetPosition) > tetherDistance;
		var outOfBounds = targetPosition.x <= -positionBounds.x || targetPosition.x >= positionBounds.x ||
							targetPosition.y <= -positionBounds.y || targetPosition.y >= positionBounds.y;

		if (tileExists || tooFar || outOfBounds)
		{
			timer = 0f;
		}
		else
		{
			transform.position = targetPosition;

			if (randomDirection.x < 0 && !sprite.flipX)
			{
				sprite.flipX = true;
			}
			else if (randomDirection.x >= 0 && sprite.flipX)
			{
				sprite.flipX = false;
			}
		}

		if (timer > 0)
		{
			timer -= Time.deltaTime;
			return;
		}

		timer = Random.Range(2.2f, 11.11f);
		sprite.sortingLayerName = sortingLayers[Random.Range(0, 3)];

		randomDirection = new Vector2(Random.value, Random.value);
		if (randomDirection != Vector2.zero)
		{
			randomDirection = randomDirection.normalized;
		}

		if (Random.value < 0.5f)
		{
			randomDirection.x *= -1f;
		}
				if (Random.value < 0.5f)
		{
			randomDirection.y *= -1f;
		}
	}
}
