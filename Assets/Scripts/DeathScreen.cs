using UnityEngine;

public class DeathScreen : MonoBehaviour
{
	public Sprite[] frames;
	public float framesPerSecond = 5f;
	private PlayerCombat playerCombat;
	private SpriteRenderer sprite;
	private int frameIndex = 0;
	private float startTime;
	public bool playing = false;

	void Start()
	{
		playerCombat = GameObject.FindWithTag("Player").GetComponent<PlayerCombat>();
		sprite = GetComponent<SpriteRenderer>();
	}

	void Update()
	{
		if (!playing)
		{
			return;
		}

		int index = Mathf.FloorToInt((Time.time - startTime) * framesPerSecond) % frames.Length;

		if (frameIndex == index)
		{
			return;
		}

		frameIndex = index;
		sprite.sprite = frames[frameIndex];

		if (frameIndex == frames.Length - 1)
		{
			playing = false;
			sprite.enabled = false;
			sprite.sprite = frames[0];

			playerCombat.Respawn();
		}
	}

	public void Play()
	{
		startTime = Time.time;
		sprite.enabled = true;
		playing = true;
	}
}
