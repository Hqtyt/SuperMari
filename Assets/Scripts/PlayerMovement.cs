using System;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	private const float JumpForce = 22.5f;

	public bool canMove = true;
	public float speed = 1.0f;
	public float jumpForceOffset = 0.0f;
	public float maxGravity = 10f;
	public float jumpQueueTime = 0.75f;
	public float jumpBufferTime = 0.55f;

	private Animator animator;
	private Rigidbody2D rigidBody;
	private SpriteRenderer sprite;
	private PlayerCombat myCombatScript;
	private bool isOnFloor = false;
	private float input = 0f;
	private ContactPoint2D[] contacts;
	private float jumpQueue = 0f;
	private float jumpBuffer = 0f;
	private bool jumpQueueWaiting = false;
	private bool jumpBufferWaiting = false;
	private bool jumping = false;
	private readonly List<string> collidableTags = new() {"Foreground", "Platform", "Enemy"};

	private void Start()
	{
		animator = GetComponent<Animator>();
		rigidBody = GetComponent<Rigidbody2D>();
		sprite = GetComponent<SpriteRenderer>();
		myCombatScript = GetComponent<PlayerCombat>();

		contacts = new ContactPoint2D[15];
	}

	private void Update() {
		if (myCombatScript.Health == 0f)
		{
			return;
		}

		if (jumpQueueWaiting)
		{
			jumpQueue -= Time.deltaTime;

			if (jumpQueue <= 0)
			{
				jumpQueueWaiting = false;
			}
		}
		if (jumpBufferWaiting)
		{
			jumpBuffer -= Time.deltaTime;

			if (jumpBuffer <= 0)
			{
				jumpBufferWaiting = false;
			}
		}
		
		input = Input.GetAxisRaw("Horizontal");

		if (input != 0f)
		{
			if (input < 0f && !sprite.flipX)
			{
				sprite.flipX = true;
			}
			else if (input > 0f && sprite.flipX)
			{
				sprite.flipX = false;
			}
			
			contacts = new ContactPoint2D[15];
			rigidBody.GetContacts(contacts);

			foreach (ContactPoint2D point in contacts)
			{
				if (point.normal.x == -Math.Sign(input))
				{
					input = 0f;
					break;
				}
			}
		}

		if (Input.GetKeyDown(KeyCode.Space) && isOnFloor)
		{
			Jump();
		}
		else if (isOnFloor && jumpQueueWaiting)
		{
			Jump();
		}
		else if (Input.GetKeyDown(KeyCode.Space) && !isOnFloor && !jumping && jumpBufferWaiting)
		{
			Jump();
		}
		else if (Input.GetKeyDown(KeyCode.Space) && (!isOnFloor || jumping))
		{
			jumpQueueWaiting = true;
			jumpQueue = jumpQueueTime;
		}

		if (!isOnFloor && Math.Abs(rigidBody.velocity.y) > 0.01f)
		{
			if (rigidBody.velocity.y > 0f && animator.GetBool("Falling"))
			{
				animator.SetBool("Falling", false);
			}
			else if (rigidBody.velocity.y <= 0f && !animator.GetBool("Falling"))
			{
				animator.SetBool("Falling", true);
			}
			return;
		}

		if (!isOnFloor)
		{
			return;
		}

		if (input == 0 && animator.GetBool("Moving"))
		{
			animator.SetBool("Moving", false);
		}
		else if (input != 0 && !animator.GetBool("Moving"))
		{
			animator.SetBool("Moving", true);
		}
	}

	private void Jump() {
		jumping = true;
		jumpQueueWaiting = false;
		jumpBufferWaiting = false;
		isOnFloor = false;
		animator.SetBool("Jumping", true);
		animator.SetBool("IsOnFloor", false);

		rigidBody.velocity = new Vector2(rigidBody.velocity.x, 0f);
		rigidBody.AddForce(((JumpForce + jumpForceOffset) / 100f) * Vector2.up, ForceMode2D.Impulse);
	}

	private void FixedUpdate() {
		float yVelocity = 0f;
	
		if (!canMove || myCombatScript.Health == 0f)
		{
			rigidBody.velocity = Vector2.zero;
			return;
		}
		else if (!isOnFloor)
		{
			yVelocity = Math.Max(-maxGravity, rigidBody.velocity.y - 0.01f);
		}

		rigidBody.velocity = new Vector2(input * speed, yVelocity);
	}

	private void OnTriggerEnter2D(Collider2D other) {
		var collided = false;

		foreach (string tag in collidableTags)
		{
			if (other.CompareTag(tag))
			{
				collided = true;
				break;
			}
		}

		if (!collided)
		{
			return;
		}
		
		jumping = false;
		isOnFloor = true;
		animator.SetBool("Jumping", false);
		animator.SetBool("Falling", false);
		animator.SetBool("IsOnFloor", true);
	}

	private void OnTriggerExit2D(Collider2D other) {
		var collided = false;

		foreach (string tag in collidableTags)
		{
			if (other.CompareTag(tag))
			{
				collided = true;
				break;
			}
		}

		if (!collided)
		{
			return;
		}

		if (!jumping)
		{
			jumpBuffer = jumpBufferTime;
			jumpBufferWaiting = true;
		}

		isOnFloor = false;
		animator.SetBool("IsOnFloor", false);
		animator.SetBool("Falling", true);
	}
}
