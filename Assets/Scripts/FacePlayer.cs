using UnityEngine;

public class FacePlayer : MonoBehaviour
{
	public bool flipX = false;
	private GameObject playerTarget;
	private SpriteRenderer sprite;

	void Start()
	{
		playerTarget = GameObject.FindWithTag("Player");
		sprite = GetComponent<SpriteRenderer>();
	}

	void Update()
	{
		var playerDirection = (transform.position - playerTarget.transform.position).normalized;
		if (playerDirection.x >= 0 && sprite.flipX == flipX)
		{
			sprite.flipX = !flipX;
		}
		else if (playerDirection.x < 0 && sprite.flipX != flipX)
		{
			sprite.flipX = flipX;
		}
	}
}
