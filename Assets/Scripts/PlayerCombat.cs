using System;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{
	public float iFrameDuration = 1.25f;
	public float maxHealth = 100f;
	public GameObject healthBubble;
	public GameObject deathScreen;
	private DeathScreen myDeathScreen;

	private float iFrames = 0f;
	private float health = 0f;
	public float Health
	{
		get
		{
			return health;
		}
		set
		{
			healthSprite.color = new Color(1f, 0f, 0f, value / maxHealth);
			health = Math.Max(0f, value);

			if (health == 0f)
			{
				myDeathScreen.Play();
			}
		}
	}
	private SpriteRenderer sprite;
	private SpriteRenderer healthSprite;
	private readonly List<string> collidableTags = new() {"EnemyProjectile"};

	void Start()
	{
		healthSprite = healthBubble.GetComponent<SpriteRenderer>();
		sprite = GetComponent<SpriteRenderer>();
		myDeathScreen = deathScreen.GetComponent<DeathScreen>();
		Health = maxHealth;
	}

	void Update()
	{
		float delta = Time.deltaTime;

		if (iFrames > 0)
		{
			iFrames -= delta;
		}
		else if (sprite.color.a != 1f)
		{
			sprite.color = new Color(1f, 1f, 1f, 1f);
		}

		if (Health > 0f)
		{
			Health -= delta;
		}
	}

	public void Respawn()
	{
		Health = maxHealth;
		transform.position = new Vector3(-3.7f, 0f, 0f);
	}

	public void OnHit(float damage = 25f)
	{
		if (iFrames > 0 || Health == 0f)
		{
			return;
		}

		Health -= damage;

		iFrames = iFrameDuration;

		sprite.color = new Color(1f, 1f, 1f, 0.5f);
	}

	private void OnTriggerEnter2D(Collider2D other) {
		var collided = false;

		foreach (string tag in collidableTags)
		{
			if (other.CompareTag(tag))
			{
				collided = true;
				break;
			}
		}

		if (!collided)
		{
			return;
		}

		OnHit();
	}
}
