using System.Collections.Generic;
using UnityEngine;

public class WaterBullet : MonoBehaviour
{
	public float speed = 2f;
	private GameObject playerTarget;
	private Rigidbody2D rigidBody;
	private SpriteRenderer sprite;
	private Transform parentTransform;
	private readonly List<string> collidableTags = new() {"Foreground", "Wall", "Platform", "Player"};

	void Start()
	{
		playerTarget = GameObject.FindWithTag("Player");
		rigidBody = GetComponent<Rigidbody2D>();
		sprite = GetComponent<SpriteRenderer>();
		parentTransform = transform.parent;

		rigidBody.inertia = 10f;
		Fire();
	}

	private void Fire()
	{
		var playerDir =  playerTarget.transform.position - parentTransform.position;
		if (playerDir != Vector3.zero)
		{
			playerDir.Normalize();
		}

		var impulse = (333 * Mathf.Deg2Rad) * rigidBody.inertia;
		print(impulse);

		sprite.enabled = true;
		rigidBody.AddForce(playerDir * speed, ForceMode2D.Impulse);
		rigidBody.AddTorque(1000f, ForceMode2D.Impulse);
	}

	private void OnTriggerEnter2D(Collider2D other) {
		var collided = false;

		foreach (string tag in collidableTags)
		{
			if (other.CompareTag(tag))
			{
				collided = true;
				break;
			}
		}

		if (!collided)
		{
			return;
		}

		rigidBody.velocity = Vector2.zero;
		sprite.enabled = false;
		transform.localPosition = Vector3.zero;
		Fire();
	}
}
